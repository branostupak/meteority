package com.branco.android.meteority.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Url;

import java.util.List;

public interface APIInterface {
    @GET
    Call<List<MeteoriteResponse>> get(@Url String url,
                                      @Header("app_token") String app_token);
}
