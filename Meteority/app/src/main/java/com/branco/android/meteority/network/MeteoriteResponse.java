package com.branco.android.meteority.network;

import com.google.gson.annotations.SerializedName;

public class MeteoriteResponse {
    @SerializedName("id")
    private String idNasa;
    @SerializedName("name")
    private String name;
    @SerializedName("fall")
    private String fall;
    @SerializedName("mass")
    private String mass;
    @SerializedName("year")
    private String year;
    @SerializedName("recclass")
    private String recclass;
    @SerializedName("reclat")
    private String reclat;
    @SerializedName("reclong")
    private String reclong;
    @SerializedName("geolocation")
    private Geolocation geolocation;


    public class Geolocation {
        @SerializedName("coordinates")
        double[] coordinates;
        @SerializedName("type")
        String type;

        public double[] getCoordinates() {
            return coordinates;
        }
    }


    public String getIdNasa() {
        return idNasa;
    }

    public String getName() {
        return name;
    }

    public String getFall() {
        return fall;
    }

    public String getMass() {
        return mass;
    }

    public String getYear() {
        return year;
    }

    public String getRecclass() {
        return recclass;
    }

    public String getReclat() {
        return reclat;
    }

    public String getReclong() {
        return reclong;
    }

    public Geolocation getGeolocation() {
        return geolocation;
    }
}
