package com.branco.android.meteority.data;

import android.os.Parcel;
import android.os.Parcelable;
import com.branco.android.meteority.database.DatabaseHelper;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = DatabaseHelper.TABLE_METEORITE)
public class Meteorite implements Parcelable, Comparable<Meteorite>{
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String idNasa;
    @DatabaseField
    private String name;
    @DatabaseField
    private String fall;
    @DatabaseField
    private Double mass;
    @DatabaseField
    private Integer year;
    @DatabaseField
    private String recclass;
    @DatabaseField
    private double longitude;
    @DatabaseField
    private double latitude;

    public Meteorite(String idNasa, String name, String fall, double mass, int year, String recclass, double longitude, double latitude) {
        this.idNasa = idNasa;
        this.name = name;
        this.fall = fall;
        this.mass = mass;
        this.year = year;
        this.recclass = recclass;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Meteorite() {
        // no-arg constructor for ORMLite
    }

    protected Meteorite(Parcel in) {
        idNasa = in.readString();
        name = in.readString();
        fall = in.readString();
        mass = in.readDouble();
        year = in.readInt();
        recclass = in.readString();
        longitude = in.readDouble();
        latitude = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idNasa);
        dest.writeString(name);
        dest.writeString(fall);
        dest.writeDouble(mass);
        dest.writeInt(year);
        dest.writeString(recclass);
        dest.writeDouble(longitude);
        dest.writeDouble(latitude);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Meteorite> CREATOR = new Creator<Meteorite>() {
        @Override
        public Meteorite createFromParcel(Parcel in) {
            return new Meteorite(in);
        }

        @Override
        public Meteorite[] newArray(int size) {
            return new Meteorite[size];
        }
    };

    public String getIdNasa() {
        return idNasa;
    }

    public void setIdNasa(String idNasa) {
        this.idNasa = idNasa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFall() {
        return fall;
    }

    public void setFall(String fall) {
        this.fall = fall;
    }

    public Double getMass() {
        return mass;
    }

    public void setMass(Double mass) {
        this.mass = mass;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getRecclass() {
        return recclass;
    }

    public void setRecclass(String recclass) {
        this.recclass = recclass;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    @Override
    public int compareTo(Meteorite meteorite) {
        return meteorite.getMass().compareTo(this.getMass());
    }
}
