package com.branco.android.meteority.data;

import android.content.Context;
import com.branco.android.meteority.database.DatabaseHelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DataManager {
    public static final String TAG = DataManager.class.getSimpleName();

    private List<Meteorite> meteoriteList;
    private static DataManager instance = null;
    private Context context;

    private DataManager(Context context) {
        this.context = context;
        this.meteoriteList = new ArrayList<>();
    }

    public synchronized static DataManager getInstance(Context context) {
        if (instance == null) {
            instance = new DataManager(context);
        }
        return instance;
    }

    public List<Meteorite> getMeteoriteList() {
        return meteoriteList;
    }

    public void setMeteoriteList(List<Meteorite> list) {
        if (meteoriteList == null)
            this.meteoriteList = new ArrayList<>();
        this.meteoriteList = list;
        Collections.sort(this.meteoriteList);
    }

    //  *** Handling work with database ***
    public void queryMeteoritesFromDb() {
        try {
            getInstance(context).setMeteoriteList(DatabaseHelper.getInstance(context).getMeteoriteDao().queryForAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeAllMeteorites() {
        try {
            meteoriteList.clear();
            DatabaseHelper.getInstance(context).getMeteoriteDao().executeRawNoArgs("delete from " + DatabaseHelper.TABLE_METEORITE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addNewMeteorites(List<Meteorite> list) {
        try {
            meteoriteList.addAll(list);
            Collections.sort(meteoriteList);
            DatabaseHelper.getInstance(context).getMeteoriteDao().create(list);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
