package com.branco.android.meteority.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.branco.android.meteority.R;
import com.branco.android.meteority.ui.fragment.MeteoriteFragment;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    private MeteoriteFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (savedInstanceState == null) {
            fragment = new MeteoriteFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_frame, fragment).commit();
        }
    }
}
