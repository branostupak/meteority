package com.branco.android.meteority.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.branco.android.meteority.R;
import com.branco.android.meteority.data.Meteorite;

import java.util.List;

public class MeteoriteAdapter extends RecyclerView.Adapter<MeteoriteAdapter.ViewHolder> {
    public static final String TAG = MeteoriteAdapter.class.getSimpleName();

    private List<Meteorite> list;
    private Context context;
    View.OnClickListener clickListener;


    public MeteoriteAdapter(Context context, List<Meteorite> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.meteorite_list_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Meteorite meteorite = list.get(position);

        char firstLetter = meteorite.getName().charAt(0);
        TextDrawable drawable = TextDrawable.builder().buildRound(String.valueOf(firstLetter), ColorGenerator.MATERIAL.getColor(firstLetter));
        holder.imageView.setImageDrawable(drawable);
        holder.name.setText(meteorite.getName());
        holder.year.setText(context.getString(R.string.row_item_year, meteorite.getYear()));
        holder.mass.setText(context.getString(R.string.row_item_mass, meteorite.getMass()));
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemClickListener(View.OnClickListener callback) {
        clickListener = callback;
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.image_view) ImageView imageView;
        @BindView(R.id.name) TextView name;
        @BindView(R.id.year) TextView year;
        @BindView(R.id.mass) TextView mass;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onClick(view);
                }
            });
        }
    }

    public List<Meteorite> getList() {
        return list;
    }

    public void setList(List<Meteorite> list) {
        this.list = list;
    }
}
