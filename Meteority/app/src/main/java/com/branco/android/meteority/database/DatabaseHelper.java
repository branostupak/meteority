package com.branco.android.meteority.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.branco.android.meteority.R;
import com.branco.android.meteority.data.Meteorite;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper{
    public static final String TAG = DatabaseHelper.class.getSimpleName();

    private static DatabaseHelper instance = null;

    private Dao<Meteorite, Integer> meteoriteDao;

    // Database version
    private static final int DATABASE_VERSION = 1;
    // Database name
    public static final String DATABASE_NAME = "Database.db";
    // Database tables names
    public static final String TABLE_METEORITE = "Meteorite";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Meteorite.class);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            Log.e(DatabaseHelper.class.getName(), "Unable to create tables to database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
    }

    public static synchronized DatabaseHelper getInstance(Context context){
        if (instance == null){
            instance = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        return instance;
    }

    public static synchronized void destroyInstance() {
        if (instance != null) {
            OpenHelperManager.releaseHelper();
            instance = null;
        }
    }

    public Dao<Meteorite, Integer> getMeteoriteDao() throws java.sql.SQLException {
        if (meteoriteDao == null) {
            meteoriteDao = getDao(Meteorite.class);
        }
        return meteoriteDao;
    }

}
