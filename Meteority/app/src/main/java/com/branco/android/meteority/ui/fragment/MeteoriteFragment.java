package com.branco.android.meteority.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.branco.android.meteority.R;
import com.branco.android.meteority.data.DataManager;
import com.branco.android.meteority.data.Meteorite;
import com.branco.android.meteority.network.APIInterface;
import com.branco.android.meteority.network.MeteoriteResponse;
import com.branco.android.meteority.ui.MeteoriteDetail;
import com.branco.android.meteority.ui.adapter.MeteoriteAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.ArrayList;
import java.util.List;

public class MeteoriteFragment extends Fragment{
    public static final String TAG = MeteoriteFragment.class.getSimpleName();

    private MeteoriteAdapter meteoriteAdapter;
    private DataManager dataManager;

    @BindView(R.id.list_view) RecyclerView recyclerView;
    @BindView(R.id.swipe_files) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.no_items_hint) TextView noItemsHint;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meteorites, container, false);
        ButterKnife.bind(this, view);
        dataManager = DataManager.getInstance(getActivity().getApplicationContext());
        dataManager.queryMeteoritesFromDb();

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        meteoriteAdapter = new MeteoriteAdapter(getActivity(), dataManager.getMeteoriteList());
        recyclerView.setAdapter(meteoriteAdapter);

        loadDataFromNasaApi();
        setClickListeners();
        setSwipeRefreshLayout();
        handleItemsCount();
        return view;
    }

    public void setClickListeners() {
        meteoriteAdapter.setOnItemClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();
                Meteorite meteorite = meteoriteAdapter.getList().get(position);

                startMeteoriteDetailActivity(meteorite);
            }
        });
    }

    public void setSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDataFromNasaApi();
//                swipeRefreshLayout.setRefreshing(false);    // fake it
            }
        });
    }
    public void finishSwipeRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }

    public void loadDataFromNasaApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.nasa.gov/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIInterface api = retrofit.create(APIInterface.class);
        Call<List<MeteoriteResponse>> call = api.get("y77d-th95.json?$where=year>='2011-01-01T00:00:00.000'", getString(R.string.nasa_api_token));

        call.enqueue(new Callback<List<MeteoriteResponse>>() {
            @Override
            public void onResponse(Call<List<MeteoriteResponse>> call, Response<List<MeteoriteResponse>> response) {
                if (response.body() != null) {
                    List<Meteorite> temp = new ArrayList<>();
                    for (MeteoriteResponse meteoriteResponse: response.body()) {
                        Meteorite meteorite = new Meteorite(meteoriteResponse.getIdNasa(),
                                meteoriteResponse.getName(),
                                meteoriteResponse.getFall(),
                                Double.valueOf(meteoriteResponse.getMass()),
                                Integer.parseInt(meteoriteResponse.getYear().substring(0,4)),
                                meteoriteResponse.getRecclass(),
                                Double.valueOf(meteoriteResponse.getReclong()),
                                Double.valueOf(meteoriteResponse.getReclat())
                                );
                        temp.add(meteorite);
                    }

                    saveResponseFromNasaApi(temp);
                    finishSwipeRefreshing();
                }
            }
            @Override
            public void onFailure(Call<List<MeteoriteResponse>> call, Throwable t) {
                finishSwipeRefreshing();
                // handle failure...
            }
        });
    }

    private void startMeteoriteDetailActivity(Meteorite meteorite) {
        Intent intent = new Intent(getContext(), MeteoriteDetail.class);
        intent.putExtra(getString(R.string.meteorite), meteorite);
        startActivity(intent);
    }

    private void saveResponseFromNasaApi(List<Meteorite> list) {
        // in case some data changed, remove all old data and add all new fetched from Nasa API
        dataManager.removeAllMeteorites();
        dataManager.addNewMeteorites(list);
        notifyDataSetChanged();
    }

    public void notifyDataSetChanged() {
        meteoriteAdapter.notifyDataSetChanged();
        handleItemsCount();
    }

    private void handleItemsCount() {
        int meteoritesCount = dataManager.getMeteoriteList().size();
        if (meteoritesCount == 0)
            noItemsHint.setVisibility(View.VISIBLE);
        else
            noItemsHint.setVisibility(View.GONE);
    }

}
