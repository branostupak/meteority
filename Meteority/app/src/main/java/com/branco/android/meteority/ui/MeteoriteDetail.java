package com.branco.android.meteority.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.branco.android.meteority.R;
import com.branco.android.meteority.data.Meteorite;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;



public class MeteoriteDetail extends AppCompatActivity implements OnMapReadyCallback {
    public static final String TAG = MeteoriteDetail.class.getSimpleName();
    private Meteorite currentMeteorite;

    private ActionBar actionBar;



    @BindView(R.id.name) TextView name;
    @BindView(R.id.year) TextView year;
    @BindView(R.id.mass) TextView mass;
    @BindView(R.id.fall) TextView fall;
    @BindView(R.id.recclass) TextView recclass;
    @BindView(R.id.geo_location) TextView geoLocation;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meteorite_detail);
        ButterKnife.bind(this);

        Bundle data = getIntent().getExtras();
        currentMeteorite = (Meteorite) data.getParcelable(getString(R.string.meteorite));

        setTitle(currentMeteorite.getName());
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        setTexts();
        setShowHideInfobox();
    }

    public void setTexts() {
        name.setText(getString(R.string.name, currentMeteorite.getName()));
        year.setText(getString(R.string.year, currentMeteorite.getYear()));
        mass.setText(getString(R.string.mass, currentMeteorite.getMass()));
        fall.setText(getString(R.string.fall, currentMeteorite.getFall()));
        recclass.setText(getString(R.string.recclass, currentMeteorite.getRecclass()));
        geoLocation.setText(getString(R.string.geo_location, currentMeteorite.getLatitude(), currentMeteorite.getLongitude()));
    }

    public void setShowHideInfobox() {
        findViewById(R.id.hide_infobox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.infobox).setVisibility(View.GONE);
                findViewById(R.id.show_infobox).setVisibility(View.VISIBLE);
            }
        });
        findViewById(R.id.show_infobox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setVisibility(View.GONE);
                findViewById(R.id.infobox).setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng position = new LatLng(currentMeteorite.getLatitude(), currentMeteorite.getLongitude());
        googleMap.addMarker(new MarkerOptions().position(position));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(position));
    }
}
